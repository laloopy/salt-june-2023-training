## Description

The Good Dogs Project recently published a blog post about the top three commands all dog owners should know. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

There are three tasks you must complete. You'll complete your work in the `/content/blog/basic-commands.md` file.

### Fix a broken link

- [ ] Fix the broken link in the **Staying in place** section.

### Change headings that use -ing 

- [ ] Convert the headings from -ing form to simple verb form.

### Change a level four heading to level three

- [ ] Change the **Sources** heading from a level four heading to a level three heading.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.
